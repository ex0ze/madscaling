#include "IScaler.h"

IScaler::IScaler(QObject *parent) : QObject(parent)
{

}

IScaler::~IScaler()
{

}

bool IScaler::loadImage(const QString &filename)
{
    bool rc = m_image.load(filename);
    if (rc)
        m_res = QImage();
    return rc;
}

bool IScaler::loadImage(const QImage &image)
{
    m_image = image;
    if (image.isNull()) return false;
    m_res = QImage();
    return true;
}
