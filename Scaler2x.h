#ifndef SCALER2X_H
#define SCALER2X_H

#include "IScaler.h"

class Scaler2x : public IScaler
{
    Q_OBJECT
public:
    Scaler2x(QObject* parent = nullptr);
    ~Scaler2x() override = default;
    QImage getScaled() override;
};

#endif // SCALER2X_H
