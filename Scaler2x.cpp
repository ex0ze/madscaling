#include "Scaler2x.h"

Scaler2x::Scaler2x(QObject *parent) : IScaler(parent)
{

}

QImage Scaler2x::getScaled()
{
    if (!m_res.isNull()) return m_res;
    m_res = QImage(m_image.width() * 2, m_image.height() * 2, m_image.format());
    QRgb C, A, B, D;
    QRgb r1, r2, r3, r4;
    for (int h = 1; h < m_image.height() - 1; ++h) {
        for (int w = 1; w < m_image.width() - 1; ++w) {
            C = m_image.pixel(w-1, h);
            A = m_image.pixel(w,h-1);
            B = m_image.pixel(w+1,h);
            D = m_image.pixel(w,h+1);
            r1 = r2 = r3 = r4 = m_image.pixel(w,h);
            if (C == A && C != D && A != B) r1 = A;
            if (A == B && A != C && B != D) r2 = B;
            if (B == D && B != A && D != C) r4 = D;
            if (D == C && D != B && C != A) r3 = C;
            m_res.setPixel((w-1)*2,(h-1)*2,r1);
            m_res.setPixel((w-1)*2+1,(h-1)*2,r2);
            m_res.setPixel((w-1)*2,(h-1)*2+1,r3);
            m_res.setPixel((w-1)*2+1,(h-1)*2+1,r4);
        }
    }
    return m_res;
}
