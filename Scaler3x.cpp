#include "Scaler3x.h"


Scaler3x::Scaler3x(QObject *parent) : IScaler(parent)
{

}

QImage Scaler3x::getScaled()
{
    if (!m_res.isNull()) return m_res;
    m_res = QImage(m_image.width() * 3, m_image.height() * 3, m_image.format());
    QRgb A, B, C, D, E, F, G, H, I;
    QVector<QRgb> rlist(9);
    for (int h = 1; h < m_image.height() - 1; ++h) {
        for (int w = 1; w < m_image.width() - 1; ++w) {
            A = m_image.pixel(w-1, h-1);
            B = m_image.pixel(w, h-1);
            C = m_image.pixel(w+1, h-1);
            D = m_image.pixel(w-1, h);
            E = m_image.pixel(w, h);
            F = m_image.pixel(w+1, h);
            G = m_image.pixel(w-1, h+1);
            H = m_image.pixel(w, h+1);
            I = m_image.pixel(w+1, h+1);
            for (auto &e : rlist) e = E;

            if (D == B && D != H && B != F) rlist[0] = D;
            if ((D == B && D != H && B != F && E != C) || (B == F && B != D && F != H && E != A)) rlist[1] = B;
            if (B == F && B != D && F != H) rlist[2] = F;
            if ((H == D && H != F && D != B && E != A) || (D == B && D != H && B != F && E != G)) rlist[3] = D;
            rlist[4] = E;
            if ((B == F && B != D && F != H && E != I) || (F == H && F != B && H != D && E != C)) rlist[5] = F;
            if (H == D && H != F && D != B) rlist[6] = D;
            if ((F == H && F != B && H != D && E != G) || (H == D && H != F && D != B && E != I)) rlist[7] = H;
            if (F == H && F != B && H != D) rlist[8] = F;

            for (int i = 0; i < 9; i++) {
                m_res.setPixel(w*3 + (-1 + i%3),h*3 + (-1 + static_cast<int>(i/3)),rlist[i]);
            }
        }
    }
    return m_res;
}

