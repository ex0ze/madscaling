#ifndef SCALER3X_H
#define SCALER3X_H

#include "IScaler.h"
#include <QVector>

class Scaler3x : public IScaler
{
    Q_OBJECT
public:
    Scaler3x(QObject* parent = nullptr);
    ~Scaler3x() override = default;
    QImage getScaled() override;
};
#endif // SCALER3X_H
