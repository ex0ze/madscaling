#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMenu>
#include "Scaler2x.h"
#include "Scaler3x.h"

#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_loadBtn_clicked();

    void on_scale2xBtn_clicked();

    void on_scale3xBtn_clicked();

    void clearImage();

    void on_actionLoad_triggered();

    void on_actionClear_scaled_triggered();

    void on_actionSave_triggered();

private:
    Ui::MainWindow *ui;
    QImage image, res;
    Scaler2x s2x;
    Scaler3x s3x;
    QSize defaultSize;
};
#endif // MAINWINDOW_H
