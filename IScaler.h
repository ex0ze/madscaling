#ifndef ISCALER_H
#define ISCALER_H

#include <QObject>
#include <QImage>
#include <QPixmap>
#include <QString>
#include <QRgb>

class IScaler : public QObject
{
    Q_OBJECT
public:
    explicit IScaler(QObject *parent = nullptr);
    virtual ~IScaler();
    virtual QImage getScaled() = 0;
signals:

public slots:
    virtual bool loadImage(const QString& filename);
    virtual bool loadImage(const QImage& image);
protected:
    QImage m_image;
    QImage m_res;
};

#endif // ISCALER_H
