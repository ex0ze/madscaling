#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    defaultSize = this->size();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_loadBtn_clicked()
{
    auto dir = QFileDialog::getOpenFileName(this, "Choose an image", QDir::currentPath(), "Images (*.png *.jpg *.bmp *.xpm);;All files(*)");
    if (image.load(dir))
        ui->baseImage->setPixmap(QPixmap::fromImage(image));
}

void MainWindow::on_scale2xBtn_clicked()
{
    if (image.isNull()) return;
    if (res.isNull()) {
        s2x.loadImage(image);
        res = s2x.getScaled();
    }
    else {
        s2x.loadImage(res);
        res = s2x.getScaled();
    }
    ui->scaledImage->setPixmap(QPixmap::fromImage(res));
}

void MainWindow::on_scale3xBtn_clicked()
{
    if (image.isNull()) return;
    if (res.isNull()) {
        s3x.loadImage(image);
        res = s3x.getScaled();
    }
    else {
        s3x.loadImage(res);
        res = s3x.getScaled();
    }
    ui->scaledImage->setPixmap(QPixmap::fromImage(res));
}

void MainWindow::clearImage()
{
    ui->scaledImage->setText("Empty");
    res = QImage();
}

void MainWindow::on_actionLoad_triggered()
{
    on_loadBtn_clicked();
}

void MainWindow::on_actionClear_scaled_triggered()
{
    clearImage();
    this->resize(defaultSize);
}

void MainWindow::on_actionSave_triggered()
{
    if (res.isNull()) return;
    QString saveFilename = QFileDialog::getSaveFileName(this, "Save image", QDir::currentPath(), "Images (*.png)");
    if (!saveFilename.isEmpty()) {
        res.save(saveFilename);
    }
}
